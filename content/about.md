---
title: Good Phone Card
date: ''

---

Have a cell phone plan with local calling only? Smart move to save on monthly costs. 

Use a reliable phone card for your long-distance calls.


It makes use of a variety of open source projects including:

* VOIP calling cards
* No WIFI, or local-calling phone plan?
* What to look for in a calling card	


Get rid of your landline and use an [Obi Hai] or [Oomla] to make calls for free.
Here's how.

